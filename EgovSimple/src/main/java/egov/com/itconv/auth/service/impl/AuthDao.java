package egov.com.itconv.auth.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Repository
public interface AuthDao {

	// 사용자 정보 조회 (로그인 체크용도로 사용 )
	public EgovMap selectMemberInfoForLogin(Map param);

	// 사용자의 비밀번호를 업데이트한다.
	public int updatePassword(Map param);

	// 해당 계정의 프로그램 아이디에 대한 버튼 권한을 가져온다.
	public EgovMap selectBtnAuth(EgovMap authParam);

}
