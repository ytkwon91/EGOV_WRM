package egov.com.itconv.auth.vo;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import egov.com.itconv.util.GsonFactory;
import egovframework.rte.psl.dataaccess.util.EgovMap;


/*	@Scope(value = "session")
 * 스프링의 빈은 디폴트로 singleton 스코프로 생성이 된다.
 * 
 * 특정 타입의 빈을 하나만 만들어 두고 공유해서 사용하기때문에 빈에 상태를 저장하는 코드를 작성하는 것은 매우 위험한 상황을 초래할 수 있으므로
 * 이럴 경우에는 프로토타입의 빈을 생성해야한다. 프로토타입은 빈 객체를 가져올때마다 새로운 빈 객체를 생성해준다.
 * 
 * 스코프 빈으로는 웹어플리케이션에서 사용할 수 있는 request, session, global session 스코프를 가진 빈들도 있다.
 * request 스코프는 웹에서 사용자 등록 등의 기능에서 위저드 화면처럼 다중 페이지를 처리할 경우에 유용하다.
 * session 스코프는 세션범위 내에서 유일한 빈을 생성해 준다.
 * global session은 포틀릿을 지원하는 컨텍스트에서만 적용이 가능하다.
 * 
 * 현재 로직을 작성하는데 있어서 필요한 부분이 session 스코프이기때문에 session 스코프에 대해서만 정리를 하겠다.
 * 기존의 HttpSession 과 Session 스코프의 빈을 사용하는 것은 저장하고자 하는 데이터가 세션 단위로 저장된다는 기본 개념은 동일하지만
 * HttpSession 을 이용하는 경우는 HttpServletRequest 객체가 필요하다.
 * 서비스 레이어에 Presentation 레이어 객체인 HttpServletRequest를 파라미터로 넘기는 것은 적절해 보이지 않는다.
 * 
 * 그렇지 않으려면 컨트롤러에서 세션에 저장된 객체를 읽어 낸 후 서비스빈으로 넘길 수 도 있지만 이 보다 더 나은 방법이 session 스코프를 사용하는 방법인것 같다.
 * 
 */
@Component
@SessionScope	//@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserInfo {
	private final static Logger LOGGER = LoggerFactory.getLogger(UserInfo.class);
	private final static String[] NONE_ACCESSIBLE_FILEDS = new String[] {"LOGGER", "LOG", "NONE_ACCESSIBLE_FILEDS", "PROG_AUTHS"};
	private final Map<String, EgovMap> PROG_AUTHS = new HashMap<String, EgovMap>();	//메뉴 권한 정보를 담는다.
	
	//Columns of UAA010
	private String mUserId;
	private String mUserName;
	private String passwordEx;	//가공: 앞자리만 표시
	private String seFlag;
	private String useSect;
	private String progId;
	private String updateDt;	//DATE
	private String userId;
	private String empNo;
	private String compCd;
	private String sectCd;
	private String enterChk;
	private String wkShopCd;	//소속작업장
	private String whType;	//물류소속창고구분
	private String whCd;	//물류소속창고
	private String loctCd;	//물류소속보관장소
	private String wkEmpNm;	//작업자명
	private String whTypeMove;	//이동창고구분
	private String whCdMove;	//이동창고
	private String loctCdMove;	//이동창고로케이션
	//Column 'NAME' of AIA010
	private String empName;
	
	
	//Constructor
	public UserInfo() {
		init();
	}
	
	
	//Getters And Setters
	public String getmUserId() {
		return mUserId;
	}
	public void setmUserId(String mUserId) {
		this.mUserId = mUserId;
	}
	public String getmUserName() {
		return mUserName;
	}
	public void setmUserName(String mUserName) {
		this.mUserName = mUserName;
	}
	public String getEmpNo() {
		return empNo;
	}
	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	


	/**
	 * setAllFields - UserInfo의 모든 필드를 매개변수 값으로 초기화한다.
	 * 
	 * @date 2021.02.16
	 * @param valueTo null 또는 ""
	 * @returns void
	 * @author 권영택
	 * @example setAllFields("");
	 */
	private void setAllFields(Object valueTo) {
		// Reflection으로 Object Field에 접근한다.
		Field[] declaredFields = this.getClass().getDeclaredFields();
		
		Arrays.stream( declaredFields ).forEach(field -> {
			if (StringUtils.equalsAnyIgnoreCase(field.getName(), NONE_ACCESSIBLE_FILEDS)) {
				return;	//Method Break.
			}
			
			// private Field일 경우 접근을 허용한다.
			field.setAccessible(true);
			
			try {
				// Field Value를 참조한다.
				field.set(this, valueTo);
			} catch(Exception e) {
				//Maybe not Accessible or more.
			}
		});	//each
	}


	/**
	 * init - UserInfo의 모든 필드를 ""로 초기화한다.
	 * 
	 * @date 2021.02.16
	 * @param 
	 * @returns void
	 * @author 권영택
	 * @example userInfo.init();
	 */
	public void init() {
		setAllFields("");
	}


	/**
	 * setUserInfo - UserInfo의 모든 필드를 memberMap으로 초기화한다.
	 * 
	 * @date 2021.02.16
	 * @param memberMap
	 * @returns void
	 * @author 권영택
	 * @example userInfo.setUserInfo(memberMap);
	 */
	public void setUserInfo(EgovMap memberMap) {
		if (memberMap == null || memberMap.isEmpty()) {
			return;
		}
		
		// Reflection으로 Object Field에 접근한다.
		Field[] declaredFields = this.getClass().getDeclaredFields();
		
		
		Arrays.stream( declaredFields ).forEach(field -> {
			if (StringUtils.equalsAnyIgnoreCase(field.getName(), NONE_ACCESSIBLE_FILEDS)) {
				return;	//Method Break.
			}
			// private Field일 경우 접근을 허용한다.
			field.setAccessible(true);
			
			try {
				Object objValue = memberMap.get( field.getName() );
				String value = Objects.toString(objValue, "");
				
				// Field Value를 참조한다.
				field.set(this, value);
			} catch(Exception e) {
				e.printStackTrace();
				//Maybe not Accessible or more.
			}
		});	//each
	}


	/**
	 * bindUserInfo - UserInfo의 모든 필드를 session에 세팅한다.
	 * 
	 * @date 2021.02.16
	 * @param session
	 * @returns void
	 * @author 권영택
	 * @example userInfo.bindUserInfo(session);
	 */
	public void bindUserInfo(HttpSession session) {
		if (session == null) {
			return;
		}
		
		// Reflection으로 Object Field에 접근한다.
		Field[] declaredFields = this.getClass().getDeclaredFields();
		
		
		Arrays.stream( declaredFields ).forEach(field -> {
			if (StringUtils.equalsAnyIgnoreCase(field.getName(), NONE_ACCESSIBLE_FILEDS)) {
				return;	//Method Break.
			}
			// private Field일 경우 접근을 허용한다.
			field.setAccessible(true);
			
			try {
				// Field Value를 참조한다.
				Object fieldValue = field.get(this);
				
				String value = Objects.toString(fieldValue, "");
				session.setAttribute(field.getName(), value);
			} catch(Exception e) {
				e.printStackTrace();
				//Maybe not Accessible or more.
			}
		});	//each
	}


	/**
	 * getMap - UserInfo의 모든 필드를 Map에 담아 return한다
	 * 
	 * @date 2021.02.16
	 * @param 
	 * @returns EgovMap
	 * @author 권영택
	 * @example EgovMap map = userInfo.getEgovMap();
	 */
	public EgovMap getEgovMap() {
		// Reflection으로 Object Field에 접근한다.
		Field[] declaredFields = this.getClass().getDeclaredFields();
		
		EgovMap map = new EgovMap();	//EgovMap은 key 값이 무조건 소문자 카멜케이스로 담는 점 주의.
		
		Arrays.stream( declaredFields ).forEach(field -> {
			if (StringUtils.equalsAnyIgnoreCase(field.getName(), NONE_ACCESSIBLE_FILEDS)) {
				return;	//Method Break.
			}
			
			// private Field일 경우 접근을 허용한다.
			field.setAccessible(true);
			
			try {
				// Field Value를 참조한다.
				Object fieldValue = field.get(this);
				
				String value = Objects.toString(fieldValue, "");
				map.put(field.getName(), value);
			} catch(Exception e) {
				e.printStackTrace();
				//Maybe not Accessible or more.
			}
		});	//each
		
		return map;
	}
	
	/**
	 * getEmpInfo - side.xml의 DataCollection - dma_defInfo에 필요한 정보를 담는 값
	 * 
	 * @date 2021.02.16
	 * @param 
	 * @returns EgovMap
	 * @author 권영택
	 * @example EgovMap map = userInfo.getDmaDefInfo();
	 */
	public EgovMap getEmpInfo() {
		//EgovMap은 key 값이 무조건 소문자 카멜케이스로 담는 점 주의.
		EgovMap map = new EgovMap();
		
		map.put("empNo", this.empNo);
		map.put("empName", this.empName);
		
		return map;
	}


	/**
	 * bindProgAuth - mProgId 버튼 권한 정보를 저장한다.
	 * 
	 * @date 2021.02.21
	 * @param String mProgId, EgovMap btnAuth
	 * @returns EgovMap
	 * @author 권영택
	 * @example userInfo.bindProgAuth(mProgId, btnAuth);
	 */
	public void bindProgAuth(String mProgId, EgovMap btnAuth) {
		PROG_AUTHS.put(mProgId, btnAuth);
	}


	public boolean isLogined() {
		return StringUtils.isNoneBlank( this.mUserId );
	}


	public String getMainLayoutCode() {
		return "T";
	}


	@Override
	public String toString() {
		return GsonFactory.getGson(LOGGER).toJson(this);
	}
}
