package egov.com.itconv.auth.ctr;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import javax.servlet.http.*;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

import egov.com.itconv.auth.service.AuthService;
import egov.com.itconv.auth.vo.UserInfo;
import egov.com.itconv.util.EgovResourceCloseHelper;
import egov.com.itconv.util.MvcResult;
import egov.com.itconv.web.common.service.CommonService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Controller
@RequestMapping(value = "/auth")
public class AuthController {

	private final static Logger LOGGER = LoggerFactory.getLogger(AuthController.class);
	
	@Autowired
	private AuthService loginService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private AuthService authService;

	@Autowired
	private UserInfo userInfo;
	
	@Autowired
	WebApplicationContext context;

	/**
	 * logout session 삭제 성공 : redirect로 기본 페이지 이동. session 삭제 오류 : 기존 화면으로 오류 메세지 전송
	 * 
	 * @date 2017.12.22
	 * @returns modelAndView
	 * @author Inswave Systems
	 * @example
	 */
	@RequestMapping(value = "/logout")
	public @ResponseBody Map<String, Object> logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
		MvcResult result = new MvcResult();
		try {
			result.setStatusMsg(MvcResult.STATUS_SUCESS, "정상적으로 로그아웃 되었습니다.");
		} catch (Exception ex) {
			ex.printStackTrace();
			result.setMsg(MvcResult.STATUS_ERROR, "로그아웃 도중 오류가 발생하였습니다.");
		} finally {
			request.getSession().invalidate();
			userInfo.init();
		}
		return result.getResult();
	}

	/**
	 * login - 요청받은 아이디, 비밀번호를 회원DB와 비교한다.
	 * 
	 * @date 2017.12.22
	 * @param dma_loginCheck { EMP_CD:"사원코드", PASSWORD:"비밀번호" }
	 * @returns mv dma_resLoginCheck { EMP_CD:"사원코드", EMP_NM:"사원명", LOGIN:"상태" }
	 * @author Inswave Systems
	 * @example
	 */
	@RequestMapping(value = "/login")
	public @ResponseBody Map<String, Object> login(@RequestBody Map<String, Object> param
			, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		String status = null;
		Map<String, Object> loginParam = null;

		MvcResult result = new MvcResult();
		try {
			// loginParam은 param(EMP_CD/PW)의 값을 꺼내는 용도
			loginParam = (Map<String, Object>) param.get("loginCheck");

			status = loginService.selectMemberInfoForLogin(loginParam);
			
			// 로그인 성공
			if (StringUtils.equals("success", status)) {
				// 메뉴 정보 가져오기
				List<EgovMap> sessionMList = commonService.selectMenuList( userInfo.getEmpInfo() );
				LOGGER.debug("login - sessionMList : \n{}\n", sessionMList);
				session.setAttribute("MENU_LIST", (List<EgovMap>) sessionMList);

				result.setStatusMsg(MvcResult.STATUS_SUCESS, "로그인 성공");
			} else if (status.equals("error")) {
				result.setData("errorItem", "PASSWORD");
				result.setMsg(MvcResult.STATUS_WARNING, "로그인 실패(패스워드 불일치)");
			} else {
				result.setData("errorItem", "ID");
				result.setMsg(MvcResult.STATUS_WARNING, "사용자 정보가 존재하지 않습니다.");
			}
		} catch (Exception ex) {// DB커넥션 없음
			ex.printStackTrace();
			result.setMsg(MvcResult.STATUS_ERROR, "처리도중 시스템 오류가 발생하였습니다.");
		}
		return result.getResult();
	}
	
	/**
	 * 로그인한 사용자의 비밀번호를 변경한다.
	 * 
	 * @date 2018.11.29
	 * @param dma_password { PASSWORD: "현재 비밀번호", NEW_PASSWORD: "새로운 비밀번호", RETRY_PASSWORD: "새로운 비밀번호(재입력)" }
	 * @returns mv dlt_result { FOCUS:"포커스를 이동할 컬럼 아이디" }
	 * @author Inswave Systems
	 * @example
	 */
	@RequestMapping("/updatePassword")
	public @ResponseBody Map<String, Object> updatePassword(@RequestBody Map<String, Object> param) {
		MvcResult result = new MvcResult();
		
		try {
			Map passwordMap = (Map) param.get("dma_password");
			boolean checkCurrPassword = false;
			
			LOGGER.info("********	미완성 기능	**************	updatePassword()");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************");
			LOGGER.info("********	미완성 기능	**************\n\n");
			
//			// 시스템 관리자인 경우에는 현재 비밀번호 체크를 하지 않고 비밀번호를 변경한다.
//			if (userInfo.getIsAdmin()) {
//				checkCurrPassword = true;
//				
//			// 일반 사용자인 경우에는 현재 비밀번호를 체크하고 비밀번호를 변경한다.
//			} else {
//				Map memberMap = loginService.selectMemberInfoForLogin(passwordMap);
//				String status = (String) memberMap.get("LOGIN");
//			   
//				// 현재 비밀번호 정상 입력 여부 확인
//				if (status.equals("success")) {
//					checkCurrPassword = true;
//				} else {
//					Map resultMap = new HashMap<String, Object>();
//					// TODO : FOCUS 정보가 정상적으로 Response에 담기지 않음
//					resultMap.put("FOCUS", "PASSWORD");
//					result.setData("dma_result", resultMap);
//					result.setStatusMsg(result.STATUS_ERROR, "현재 비밀번호를 잘못 입력하셨습니다.");
//					return result.getResult();
//				}
//			}
//			
//			String newPassword = (String) passwordMap.get("NEW_PASSWORD");
//			String retryPassword = (String) passwordMap.get("RETRY_PASSWORD");
//			
//			if (newPassword.equals(retryPassword)) {
//				loginService.updatePassword(passwordMap);
//				result.setStatusMsg(result.STATUS_SUCESS, "비밀번호 변경에 성공했습니다.");
//			} else {
//				Map resultMap = new HashMap<String, Object>();
//				resultMap.put("FOCUS", "NEW_PASSWORD");
//				result.setData("dma_result", resultMap);
//				result.setStatusMsg(result.STATUS_ERROR, "신규 비밀번호와 신규 비밀번호(재입력) 항목의 비밀번호가 다르게 입력 되었습니다.");
//			}

		} catch (Exception ex) {
			result.setMsg(MvcResult.STATUS_ERROR, "비밀번호 변경 중 오류가 발생했습니다.");
		}

		return result.getResult();
	}

	
	/**
	 * 로그인한 사용자 아이디와 {mProgId}로 버튼 권한 정보를 조회하여
	 * 프로그램명을 표시하고 권한이 없는 세부 버튼은 가려서
	 * XML을 내려받아준다.
	 * 
	 * @date 2022.02.22
	 * @param 
	 * @returns 가공된 /auth/xml/wf_topBtns.xml
	 * @author 권영택
	 * @example
	 */
	@RequestMapping("/xml/btns/{mProgId}")
	public void xmlBtns(@PathVariable String mProgId
			, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		LOGGER.info("|||||||||||||||||| xmlTest.");
		LOGGER.debug("mProgId : {}", mProgId);
		
		//Writer 객체 얻기.
		response.addHeader("Accept-Ranges", "bytes");
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Connection", "Keep-Alive");
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/xml");
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException ioe) {
		}
		
		
		if(StringUtils.isBlank(mProgId)) {
			writer.write( getNoneAuthXml( "" ) );
			return;
		}
		
		
		try {
			//0. 권한 정보를 가져온다.
			EgovMap authParam = new EgovMap();
			authParam.put("mUserId", userInfo.getmUserId());
			authParam.put("mProgId", mProgId);
			authParam.put("userGrop", "A");
			EgovMap btnAuth = authService.selectBtnAuth( authParam );
			
			//0-1. 메뉴 권한 정보를 세션info 정보에 담는다.
			userInfo.bindProgAuth(mProgId, btnAuth);
			
			//1. Application Context를 가져온다.
			//2. resource 경로를 가져온다.
			String absolutePath = context.getServletContext().getRealPath("/auth/xml");
			//3. 해당 경로의 파일명을 가져온다.
			Path path = Paths.get(absolutePath, "wf_topBtns.xml");
			LOGGER.debug("path :: {}", path);
			
			//4. 파일을 nio 라이브러리를 이용해 String으로 읽는다.
			List<String> strLines = null;
			try {
				strLines = Files.readAllLines( path );	//NunNull.
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				writer.write( getNoneAuthXml() );
				return;
			}
			
			/*	Collection List를 합치는 2가지 방법
			 * 1. String 라이브러리 이용
			 * 	- String result = String.join("", strLines);
			 * 2. Stream 라이브러리 이용
			 * 	- String result = strLines.stream().reduce("", (total, str) -> total + str);
			 * */
			final String LINE = "__LINE__";
			String xml = String.join(LINE, strLines);
			//LOGGER.debug("xml :: {}", xml);
			
			//5. Regular Express를 이용하여 해당 id의 트리거 추출
			/*	Regex. Ex. id="topbtn_Clear" 트리거 추출
			 *	<\s*xf:trigger[^>]*id="topbtn_Clear"[^>]*>(.*?)<\s*\/\s*xf:trigger\s*>
			 *	
			 *	[풀이]
			 *	<					# Literally
			 *	\s*					# Matches zero or more : Whitespace (spaces, tabs, line breaks)
			 *	xf:trigger			# Literally
			 *	[^>]*				# Matches zero or more : Any character that is not '>'
			 *	id="topbtn_Clear"	# Literally
			 *	[^>]*				# Matches zero or more : Any character that is not '>'
			 *	>					# Literally
			 *	(.*?				# Start lookahead, Matches zero or more : Any, not line breaks.
			 *	)					# End lookahead
			 *	<					# Literally
			 *	\s*					# Matches zero or more : Whitespace (spaces, tabs, line breaks)
			 *	\/					# Symbol for '/'
			 *	\s*					# Matches zero or more : Whitespace (spaces, tabs, line breaks)
			 *	xf:trigger			# Literally
			 *	\s*					# Matches zero or more : Whitespace (spaces, tabs, line breaks)
			 *	>					# Literally
			 * */
			
			//권한에 따른 버튼 처리
			final String tagName = "xf:trigger";
			Set<String> kList = btnAuth.keySet();
			xml = kList.stream().reduce(xml, (accumulator, key) -> {
				//LOGGER.debug("[{}] accumulator :: {} ", key, accumulator);
				String auth = Objects.toString(btnAuth.get( key ), "");
				
				boolean unauthenticated = !StringUtils.equalsAnyIgnoreCase(auth, "1", "Y");
				//LOGGER.debug("unauthenticated :: {} ", unauthenticated);
				if(unauthenticated) {
					String id = "topbtn_" + StringUtils.capitalize( key );
					//LOGGER.debug("id :: {} ", id);
					String regex = ""
							+ "<\\s*" + tagName + "[^>]*id=\"" + id + "\"[^>]*>"
							+	 "(.*?)"
							+ "<\\s*\\/\\s*" + tagName + "\\s*>";
					//LOGGER.debug("matches :: {} ", Pattern.compile( regex ).matcher(accumulator).matches());
					accumulator = accumulator.replaceAll(regex, String.format("<!-- [%s] 권한 제거 -->", key));
				}
				return accumulator;
			});
			
			//프로그램명 세팅
			String progName = Objects.toString(btnAuth.get("progName"), "프로그램명 조회 실패");
			xml = xml.replace("label=\"__PROG_NAME__\"", String.format("label=\"%s\"", progName));
			
			//라인 살리기
			xml = xml.replace(LINE, "\n");

			writer.write( xml );
		} catch(Exception e) {
			e.printStackTrace();
			writer.write( getNoneAuthXml("시스템 에러입니다.") );
		} finally {
			if(writer != null) {
				writer.flush();
				EgovResourceCloseHelper.close( writer );
			}
		}
	}	//End of xmlBtns()
	

	public static String getNoneAuthXml() {
		return getNoneAuthXml(null);
	}
	
	public static String getNoneAuthXml(String msg) {
		String label = Objects.toString(msg, " (권한이 없습니다. com.authBtns(); 이용하세요.)");
		
		return new StringBuilder().append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
			.append("\n").append("<?xml-stylesheet href=\"/cm/css/all.css\" type=\"text/css\"?>")
			.append("\n").append("<?xml-stylesheet href=\"/cm/css/content.css\" type=\"text/css\"?>")
			.append("\n").append("<?xml-stylesheet href=\"/cm/css/responsive.css\" type=\"text/css\"?>")
			.append("\n").append("<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:ev=\"http://www.w3.org/2001/xml-events\" xmlns:w2=\"http://www.inswave.com/websquare\" xmlns:xf=\"http://www.w3.org/2002/xforms\">")
			.append("\n").append("	<head meta_vertical_guides=\"\" meta_horizontal_guides=\"\">")
			.append("\n").append("		<w2:type>DEFAULT</w2:type>")
			.append("\n").append("		<script type=\"text/javascript\" lazy=\"false\"><![CDATA[")
			.append("\n").append("scwin.onpageload = function() {")
			.append("\n").append("	var progName = $p.top().wfm_side.getWindow().inp_progName.getValue();")
			.append("\n").append("	spn_progName.setValue(progName);")
			.append("\n").append("};]]></script>")
			.append("\n").append("	</head>")
			.append("\n").append("	<body ev:onpageload=\"scwin.onpageload\">")
			.append("\n").append("		<xf:group style=\"height:30px;\" id=\"wfTopBtns_grp_Main\" class=\"\">")
			.append("\n").append("			<w2:span label=\"프로그램명\" style=\"font-size: 13pt;padding-left: 10px;\" id=\"spn_progName\"></w2:span>")
			.append("\n").append("			<w2:span id=\"span2\" label=\"").append( label ).append("\" style=\"font-size: 13pt;font-weight: bold;\"></w2:span>")
			.append("\n").append("		</xf:group>")
			.append("\n").append("	</body>")
			.append("\n").append("</html>")
			.toString();
	}
}
