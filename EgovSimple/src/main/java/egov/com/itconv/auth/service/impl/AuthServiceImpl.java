package egov.com.itconv.auth.service.impl;

import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import egov.com.itconv.auth.service.AuthService;
import egov.com.itconv.auth.vo.UserInfo;
import egov.com.itconv.util.PageURIUtil;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Service
public class AuthServiceImpl implements AuthService {
	private final static Logger LOGGER = LoggerFactory.getLogger(AuthServiceImpl.class);

	@Autowired
	private AuthDao authDao;

	//private String adminId = "100001";
	
	@Autowired
	private UserInfo userInfo;

	/**
	 * 사용자 정보 조회 (로그인 체크용도로 사용 )
	 */
	@Override
	public String selectMemberInfoForLogin(Map<String, Object> param) {
		EgovMap memberMap = authDao.selectMemberInfoForLogin(param);
		LOGGER.debug("selectMemberInfoForLogin - memberMap : {}\n", memberMap);
		
		userInfo.init();
		if (memberMap == null) {
			// 사용자가 존재하지 않을 경우
			
			return "notexist";
		} else {
			// 사용자가 존재할 경우
			String password = Objects.toString(memberMap.get("password"), "");
			String loginPwd = Objects.toString(param.get("loginPwd"), "");
			
			boolean loginOk = StringUtils.equals(password, loginPwd);
			
			if (loginOk) {
				//login 성공
				userInfo.setUserInfo(memberMap);
			}
			return loginOk ? "success" : "error";
		}
	}
	
	/**
	 * 사용자의 비밀번호를 업데이트한다.
	 */
	@Override
	public int updatePassword(Map param) {
		return authDao.updatePassword(param);
	}
	
	@Override
	public boolean isAdmin(String userId) {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * 로그인 페이지 Url을 반환한다.
	 * 
	 * @param w2xPath w2xPath 파라미터
	 * @return 로그인 페이지 Url
	 */
	@Override
	public String getLoginPage(String w2xPath) {
		String movePage = w2xPath;

		// session이 없을 경우 login 화면으로 이동.
		if (!userInfo.isLogined()) {
			// session이 있고 w2xPath가 없을 경우 index화면으로 이동.
			movePage = PageURIUtil.getLoginPage();
		} else {
			if (movePage == null) {
				// DB 설정조회 초기 page 구성
				movePage = PageURIUtil.getIndexPageURI(userInfo.getMainLayoutCode());

				// DB에 값이 저장되어 있지 않은 경우 기본 index화면으로 이동
				if (movePage == null) {
					movePage = PageURIUtil.getIndexPageURI();
				}
			}
		}
		return movePage;
	}

	/**
	 * 해당 계정의 프로그램 아이디에 대한 버튼 권한을 가져온다.
	 */
	@Override
	public EgovMap selectBtnAuth(EgovMap authParam) {
		return authDao.selectBtnAuth( authParam );
	}
}
