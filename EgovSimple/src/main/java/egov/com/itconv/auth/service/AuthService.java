package egov.com.itconv.auth.service;

import java.util.Map;

import egovframework.rte.psl.dataaccess.util.EgovMap;

public interface AuthService {

	// 사용자 정보 조회 (로그인 체크용도로 사용 )
	public String selectMemberInfoForLogin(Map<String, Object> param);

	// 해당 사용자 아이디가 관리자 아이디인지를 검사한다.
	public boolean isAdmin(String userId);
	
	// 사용자의 비밀번호를 업데이트한다.
	public int updatePassword(Map<String, Object> param);
	
	// 로그인 페이지 Url을 반환한다.
	public String getLoginPage(String w2xPath);

	// 해당 계정의 프로그램 아이디별 버튼 권한을 얻어온다.
	public EgovMap selectBtnAuth(EgovMap authParam);
}
