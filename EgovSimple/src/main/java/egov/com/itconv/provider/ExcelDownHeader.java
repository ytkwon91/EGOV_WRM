package egov.com.itconv.provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import websquare.http.controller.grid.excel.write.IExternalGridDataProvider;
import websquare.util.XMLUtil;

public class ExcelDownHeader implements IExternalGridDataProvider {
	private final static Logger LOGGER = LoggerFactory.getLogger(ExcelDownHeader.class);

	/** 참조
	 * https://inswave.com/confluence/pages/viewpage.action?pageId=10813647
	 */
	@Override
	public String[] getData(Document requestObj) throws Exception {
		LOGGER.info("requestObj.");
		LOGGER.debug("{}", requestObj);
		
		String[] result = new String[] { "" };
		if (requestObj == null) {
			//조회된 데이터를 문자열 배열로 return
			return result;
		}
		//requestObj는 options.providerRequestXML
		LOGGER.debug("Provider Request XML: " + XMLUtil.indent(requestObj));
		
		//전달받은 XML문자열에서 key값을 추출 가능
		String key = XMLUtil.getText(requestObj, "key");
		LOGGER.debug("key: " + key);
		
		//조회된 데이터를 문자열 배열로 return
		return result;
	}

}
