package egov.com.itconv.cmm;

public interface Globals {
	//OS 유형
    public String OS_TYPE = EgovProperties.getProperty("Globals.OsType");
    //DB 유형
    public String DB_TYPE = EgovProperties.getProperty("Globals.DbType");
    //권한 인증방식
    public String AUTH = EgovProperties.getProperty("Globals.Auth");
    //메인 페이지
    public String MAIN_PAGE = EgovProperties.getProperty("Globals.MainPage");
    //ShellFile 경로
    public String SHELL_FILE_PATH = EgovProperties.getPathProperty("Globals.ShellFilePath");
    //퍼로퍼티 파일 위치
    public String CONF_PATH = EgovProperties.getPathProperty("Globals.ConfPath");
    //Server정보 프로퍼티 위치
    public String SERVER_CONF_PATH = EgovProperties.getPathProperty("Globals.ServerConfPath");
    //Client정보 프로퍼티 위치
    public String CLIENT_CONF_PATH = EgovProperties.getPathProperty("Globals.ClientConfPath");
    //파일포맷 정보 프로퍼티 위치
    public String FILE_FORMAT_PATH = EgovProperties.getPathProperty("Globals.FileFormatPath");

    //파일 업로드 원 파일명
	public String ORIGIN_FILE_NM = "originalFileName";
	//파일 확장자
	public String FILE_EXT = "fileExtension";
	//파일크기
	public String FILE_SIZE = "fileSize";
	//업로드된 파일명
	public String UPLOAD_FILE_NM = "uploadFileName";
	//파일경로
	public String FILE_PATH = "filePath";

	//메일발송요청 XML파일경로
	public String MAIL_REQUEST_PATH = EgovProperties.getPathProperty("Globals.MailRequestPath");
	//메일발송응답 XML파일경로
	public String MAIL_RESPONSE_PATH = EgovProperties.getPathProperty("Globals.MailRResponsePath");

	// G4C 연결용 IP (localhost)
	public String LOCAL_IP = EgovProperties.getProperty("Globals.LocalIp");

	//SMS 정보 프로퍼티 위치
	public String SMSDB_CONF_PATH = EgovProperties.getPathProperty("Globals.SmsDbConfPath");
}
