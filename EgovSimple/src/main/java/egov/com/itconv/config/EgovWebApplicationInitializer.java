package egov.com.itconv.config;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

import egov.com.itconv.cmm.Globals;
import egov.com.itconv.context.EgovWebServletContextListener;
import egov.com.itconv.filter.CrossScriptingFilter;
//import egov.com.itconv.filter.EgovLoginPolicyFilter;
//import egov.com.itconv.filter.EgovSpringSecurityLogoutFilter;
import websquare.http.DefaultRequestDispatcher;
import websquare.http.controller.JavascriptInitializer;

public class EgovWebApplicationInitializer implements WebApplicationInitializer {
	private final static Logger LOGGER = LoggerFactory.getLogger(EgovWebApplicationInitializer.class);
	
	//	'/' 은 IninController에서 받아요~
	private String[] dispatcherUrlPatterns = new String[] {
		"/" , "/popup" , "/clearCache" , "/I18N"
		, "/login/**"
		, "/authority/**"
		, "/common/**"
		, "/file/**"
		, "/main/**"
		, "/member/**"
		, "/organization/**"
		, "/menu/**"
		, "/message/**"
		, "/program/**"
	};
	private String[] authUrlPatterns = new String[] {
		"/authority/**"
		, "/common/**"
		, "/file/**"
		, "/main/**"
		, "/member/**"
		, "/organization/**"
		, "/menu/**"
		, "/message/**"
		, "/program/**"
	};
	
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		LOGGER.debug("EgovWebApplicationInitializer START-============================================");
		
		//-------------------------------------------------------------
		// Egov Web ServletContextListener 설정
		//-------------------------------------------------------------
		servletContext.addListener(new EgovWebServletContextListener());
		
		//-------------------------------------------------------------
		// Spring CharacterEncodingFilter 설정
		//-------------------------------------------------------------
		FilterRegistration.Dynamic characterEncoding = servletContext.addFilter("encodingFilter", new org.springframework.web.filter.CharacterEncodingFilter());
		characterEncoding.setInitParameter("encoding", "UTF-8");
		characterEncoding.setInitParameter("forceEncoding", "true");
		characterEncoding.addMappingForUrlPatterns(null, false, dispatcherUrlPatterns);
		//characterEncoding.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "*.do");
		
		//-------------------------------------------------------------
		// Spring ServletContextListener 설정
		//-------------------------------------------------------------
		XmlWebApplicationContext rootContext = new XmlWebApplicationContext();
		rootContext.setConfigLocations(new String[] { "classpath*:egovframework/spring/com/**/context-*.xml" });
		//rootContext.setConfigLocations(new String[] { "classpath*:egovframework/spring/com/context-*.xml","classpath*:egovframework/spring/com/*/context-*.xml" });
		rootContext.refresh();
		rootContext.start();
		
		servletContext.addListener(new ContextLoaderListener(rootContext));
		
		//-------------------------------------------------------------
		// Spring ServletContextListener 설정
		//-------------------------------------------------------------
		XmlWebApplicationContext xmlWebApplicationContext = new XmlWebApplicationContext();
		xmlWebApplicationContext.setConfigLocation("/WEB-INF/config/egovframework/springmvc/*.xml");
		ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", new DispatcherServlet(xmlWebApplicationContext));
		dispatcher.addMapping(dispatcherUrlPatterns);
		//dispatcher.setLoadOnStartup(1);
		
		
		/*	
		 *	WAS를 구동하면서 웹스퀘어 엔진을 로딩하기 위해서 JavascriptInitializer를 listener에 등록
		 *	우선 websquare-5.**.jar 등 웹 스퀘어 라이브러리가 추가되어야 해요.
		*/
		servletContext.addListener(new JavascriptInitializer());
		ServletRegistration.Dynamic websquareDispatcher = servletContext.addServlet("websquareDispatcher", new DefaultRequestDispatcher());
		websquareDispatcher.addMapping("*.wq");
		//websquareDispatcher.setLoadOnStartup(1);
		

		//-------------------------------------------------------------
	    // HTMLTagFilter의 경우는 파라미터에 대하여 XSS 오류 방지를 위한 변환을 처리합니다.
		//	- HTMLTagFilter 대신 CrossScriptingFilter 이거 쓰자!
		//-------------------------------------------------------------	
	    // HTMLTagFIlter의 경우는 JSP의 <c:out /> 등을 사용하지 못하는 특수한 상황에서 사용하시면 됩니다.
	    // (<c:out />의 경우 뷰단에서 데이터 출력시 XSS 방지 처리가 됨)
//		FilterRegistration.Dynamic htmlTagFilter = servletContext.addFilter("htmlTagFilter", new HTMLTagFilter());
//		htmlTagFilter.addMappingForUrlPatterns(null, false, "*.do");
		FilterRegistration.Dynamic xss = servletContext.addFilter("xss", new CrossScriptingFilter());
		xss.addMappingForUrlPatterns(null, false, "*.do");
		
		
		

		//-------------------------------------------------------------
	    //	Spring Security 설정
		//-------------------------------------------------------------	
		/*
		 *	권한 인증방식(dummy, session, security) - 사용자의 로그인시 인증 방식을 결정함
		 *	dummy : 더미 방식으로 사용자 권한을 인증함
		 *	session : 세션 방식으로 사용자 권한을 인증함
		 *	security : spring security 방식으로 사용자 권한을 인증함
		 */
		/**	WebSquare 사용하면서 Spring Security는 사용 하기 어려움.
		 * 구조 아키텍처 상 Filter 영역에서 제어하는 기능인데, 모든 URL이 '/' 로 동일해서 사용 불가.
		 * XHR 요청으로 Intercepter에서 처리 가능
		 * */
		/*
		String loginProcessURL = "/main/login";
		if (StringUtils.equals("dummy", Globals.AUTH)) {
			//Empty.
		} else if (StringUtils.equals("session", Globals.AUTH)) {
			//-------------------------------------------------------------
			// EgovLoginPolicyFilter 설정
			//-------------------------------------------------------------	
//			FilterRegistration.Dynamic egovLoginPolicyFilter = servletContext.addFilter("LoginPolicyFilter", new EgovLoginPolicyFilter());
//			egovLoginPolicyFilter.addMappingForUrlPatterns(null, false, loginProcessURL);
		} else if (StringUtils.equals("security", Globals.AUTH)) {
			//-------------------------------------------------------------
			// springSecurityFilterChain 설정
			//-------------------------------------------------------------		
			FilterRegistration.Dynamic springSecurityFilterChain = servletContext.addFilter("springSecurityFilterChain", new DelegatingFilterProxy());
			springSecurityFilterChain.addMappingForUrlPatterns(null, false, "*");
			//servletContext.addFilter("springSecurityFilterChain", new DelegatingFilterProxy("springSecurityFilterChain")).addMappingForUrlPatterns(null, false, "/*");

			//-------------------------------------------------------------
			// HttpSessionEventPublisher 설정
			//-------------------------------------------------------------	
			servletContext.addListener(new HttpSessionEventPublisher());
			
			//-------------------------------------------------------------
			// EgovSpringSecurityLoginFilter 설정
			//-------------------------------------------------------------
			FilterRegistration.Dynamic egovSpringSecurityLoginFilter = servletContext.addFilter("egovSpringSecurityLoginFilter", new EgovSpringSecurityLoginFilter());
			//로그인 실패시 반활 될 URL설정
			egovSpringSecurityLoginFilter.setInitParameter("loginURL", "/");
			//로그인 처리 URL설정
			egovSpringSecurityLoginFilter.setInitParameter("loginProcessURL", loginProcessURL);
			//처리 Url Pattern
			egovSpringSecurityLoginFilter.addMappingForUrlPatterns(null, false, authUrlPatterns);
			
			//-------------------------------------------------------------
			// EgovSpringSecurityLogoutFilter 설정
			//-------------------------------------------------------------	
			FilterRegistration.Dynamic egovSpringSecurityLogoutFilter = servletContext.addFilter("egovSpringSecurityLogoutFilter", new EgovSpringSecurityLogoutFilter());
			egovSpringSecurityLogoutFilter.addMappingForUrlPatterns(null, false, "/uat/uia/actionLogout.do");
		}
		*/
		
		LOGGER.debug("EgovWebApplicationInitializer END-============================================");
	}

}
