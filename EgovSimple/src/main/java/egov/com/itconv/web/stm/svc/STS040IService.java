package egov.com.itconv.web.stm.svc;

import java.util.List;
import java.util.Map;

import egovframework.rte.psl.dataaccess.util.EgovMap;

public interface STS040IService {
	//프로그램 목록 조회
	public List<EgovMap> selectSTS040I(Map<String, Object> dmaSearch);
}
