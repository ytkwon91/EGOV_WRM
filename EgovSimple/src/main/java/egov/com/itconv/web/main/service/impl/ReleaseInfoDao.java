package egov.com.itconv.web.main.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

@Repository
public interface ReleaseInfoDao {

	public List<Map<String, Object>> selectReleaseForSummary(Map param);

	// 메뉴관리 C, U, D
	public int insertRelease(Map param);

	public int deleteRelease(Map param);

	public int updateRelease(Map param);

	public Map<String, Object> selectReleasetTotalCnt();
}