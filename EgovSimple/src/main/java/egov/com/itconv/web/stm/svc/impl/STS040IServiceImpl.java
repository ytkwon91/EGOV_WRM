package egov.com.itconv.web.stm.svc.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import egov.com.itconv.web.stm.svc.STS040IService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Service("sts040iService")
public class STS040IServiceImpl implements STS040IService {
	
	@Autowired
	private STS040IDao sts040iDao;

	@Override
	public List<EgovMap> selectSTS040I(Map<String, Object> dmaSearch) {
		return sts040iDao.selectSTS040I(dmaSearch);
	}

}
