package egov.com.itconv.web.stm.svc.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import egovframework.rte.psl.dataaccess.util.EgovMap;

@Repository("sts040iDao")
public interface STS040IDao {
	//프로그램 목록 조회
	public List<EgovMap> selectSTS040I(Map<String, Object> dmaSearch);
}
