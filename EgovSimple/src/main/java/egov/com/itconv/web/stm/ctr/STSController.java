package egov.com.itconv.web.stm.ctr;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import egov.com.itconv.util.MvcResult;
import egov.com.itconv.web.stm.svc.STS040IService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Controller
@RequestMapping("/STS")
public class STSController {
	
	@Autowired
	private STS040IService sts040iService;

	@RequestMapping(value = "/STS040I/search", method = RequestMethod.POST)
	public @ResponseBody Map<String, Object> searchSTS040I(@RequestBody Map<String, Object> param
			, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws Exception {
		MvcResult result = new MvcResult();
		//
		try {
			Map<String, Object> dmaSearch = (Map<String, Object>) param.get("dma_Search");
			List<EgovMap> progList = sts040iService.selectSTS040I( dmaSearch );
			result.setData("dlt_ProgList", progList);
			
			result.setStatusMsg(result.STATUS_SUCESS, "조회가 완료되었습니다.");
		} catch (Exception ex) {
			ex.printStackTrace();
			result.setMsg(result.STATUS_ERROR, "처리 도중 오류가 발생하였습니다.");
		}
		return result.getResult();
	}
}
