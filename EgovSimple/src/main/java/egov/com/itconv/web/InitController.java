package egov.com.itconv.web;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import egov.com.itconv.auth.service.AuthService;

@Controller
public class InitController {
	private final static Logger LOGGER = LoggerFactory.getLogger(InitController.class);
	
	@Autowired
	private AuthService authService;
	
	/**
	 * 기본 Root Url 처리
	 * 
	 * @date 2017.12.22
	 * @author Inswave Systems
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String IndexBase(HttpServletRequest request, Model model) throws Exception {
		model.addAttribute("movePage", authService.getLoginPage(request.getParameter("w2xPath")));
		LOGGER.info("IndexBase().");
		return "websquare/websquare";
	}
	
	/**
	 * Popup Url 처리
	 * 
	 * @date 2017.12.22
	 * @author Inswave Systems
	 */
	@RequestMapping(value = "/popup", method = RequestMethod.GET)
	public String IndexWebSquare(HttpServletRequest request, Model model) throws Exception {
		model.addAttribute("movePage", authService.getLoginPage(request.getParameter("w2xPath")));
		return "websquare/popup";
	}
}
