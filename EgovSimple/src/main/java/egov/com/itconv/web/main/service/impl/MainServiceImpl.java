package egov.com.itconv.web.main.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import egov.com.itconv.web.main.service.MainService;
import egovframework.rte.psl.dataaccess.util.EgovMap;

@Service
public class MainServiceImpl implements MainService {

	@Autowired
	private ReleaseInfoDao releaseInfoDao;

	/**
	 * selectType=="S" : 요약본
	 */
	public List<Map<String, Object>> selectRelease(Map param) {

		List rs = null;
		String selectType = (String) param.get("selectType");

		if (selectType == "S") {
			rs = releaseInfoDao.selectReleaseForSummary(param);
		}
		return rs;
	}

	/**
	 * 여러 건의 Release관리 데이터를 변경(등록, 수정, 삭제)한다.
	 * 
	 * @param param Client 전달한 데이터 맵 객체
	 */
	@Override
	public EgovMap saveRelease(List param) {
		int iCnt = 0;
		int uCnt = 0;
		int dCnt = 0;

		for (int i = 0; i < param.size(); i++) {

			Map data = (Map) param.get(i);
			String rowStatus = (String) data.get("rowStatus");
			if (rowStatus.equals("C")) {
				iCnt += releaseInfoDao.insertRelease(data);
			} else if (rowStatus.equals("U")) {
				uCnt += releaseInfoDao.updateRelease(data);
			} else if (rowStatus.equals("D")) {
				dCnt += releaseInfoDao.deleteRelease(data);
			}
		}
		
		EgovMap result = new EgovMap();
		result.put("STATUS", "S");
		result.put("ICNT", String.valueOf(iCnt));
		result.put("UCNT", String.valueOf(uCnt));
		result.put("DCNT", String.valueOf(dCnt));
		return result;
	}

	public Map<String, Object> selectReleasetTotalCnt() {
		return releaseInfoDao.selectReleasetTotalCnt();
	}
}
