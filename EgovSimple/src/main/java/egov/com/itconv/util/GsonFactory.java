package egov.com.itconv.util;

import org.slf4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonFactory {
	
	public static Gson getGson(Logger logger) {
		if (logger == null) {
			return new Gson();
		}
		if (logger.isTraceEnabled() || logger.isDebugEnabled()) {
			return new GsonBuilder().setPrettyPrinting().create();
		}
		return new Gson();
	}
	
	public static Gson getGson() {
		return getGson(null);
	}
	
}
