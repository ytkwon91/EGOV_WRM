package egov.com.itconv.util;

public class XssUtil {
	/**
	 * jsp 파일에서, 안내 문구 적용 예..
	 * 
	 * <%@ page import="kr.or.koita.snejob.util.XssUtil" %>
	 * 
		<c:if test="${sessionScope.ssAdminPage lt 1}">	<!-- 관리자 계정으로 로그인 한것이 아니라면, -->
		<style>
		.cn-warning {line-height: 18px;}
		.cn-warning b {background-color: #EAEAEA;}
		</style>
		<div class="cn-warning" style="display: block;" >
			 - 아래와 같은 문자들은 내용에 포함하여 사용하실 수 없습니다.<br />
			 - 해당 문자가 포함되어 있으면, 빈 문자열로 치환되어 내용이 이상하게 저장될 수 있음을 유의 바랍니다.<br />
			 - 에디터 아래 영역에 <span style="font-weight: bold;" >HTML</span> 탭을 선택하여 내용을 확인하실 수 있습니다.<br />
			 - <span style="color: red;">링크, 이미지 및 아래와 같은 문구는 내용에 포함시킬 수 없습니다.</span><br />
			<%
			for(int i = 0; i < XssUtil.SEARCHES.length; i++) {
				String banStr = XssUtil.SEARCHES[i];
				if(i > 0) {%><span>, </span><%}
				%><b><%=banStr.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")%></b><%
			}	//End of LOOP
			%>
		</div>
		</c:if>
	 * 
	 * 
	<script type="text/javascript">
	var oEditors = [];
	var isEditor = false;
	function initEditor() {	//Smart Editor 높은 버전일 경우
		nhn.husky.EZCreator.createInIFrame({
			oAppRef: oEditors,
			elPlaceHolder: 'nttCn',
			sSkinURI: '/smartEditor2/SmartEditor2Skin.html',
			htParams : {
				bUseToolbar : true,				// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
				bUseVerticalResizer : true,		// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
				bUseModeChanger : true,			// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
				bSkipXssFilter : true,		// client-side xss filter 무시 여부 (true:사용하지 않음 / 그외:사용)
				//aAdditionalFontList : aAdditionalFontSet,		// 추가 글꼴 목록
				fOnBeforeUnload : function(){
					//alert("완료!");
				},
				I18N_LOCALE : 'ko_KR'
			},
			fCreator: 'createSEditor2'
		});
		isEditor = true;
	}
	</script>
	 */
	public final static String[] SEARCHES = new String[] {
		"<iframe"
	,	"iframe>"
	,	"<style"
	,	"style>"
	,	"<script"
	,	"script>"
	,	"<object"
	,	"object>"
	,	"<applet"
	,	"applet>"
	,	"<embed"
	,	"embed>"
	,	"<form"
	,	"form>"
	,	"<input"
	,	"<img"
	,	"<a"
	,	"</a>"
	
	,	"<iframe".replace("<", "&lt;").replace(">", "&gt;")
	,	"iframe>".replace("<", "&lt;").replace(">", "&gt;")
	,	"<style".replace("<", "&lt;").replace(">", "&gt;")
	,	"style>".replace("<", "&lt;").replace(">", "&gt;")
	,	"<script".replace("<", "&lt;").replace(">", "&gt;")
	,	"script>".replace("<", "&lt;").replace(">", "&gt;")
	,	"<object".replace("<", "&lt;").replace(">", "&gt;")
	,	"object>".replace("<", "&lt;").replace(">", "&gt;")
	,	"<applet".replace("<", "&lt;").replace(">", "&gt;")
	,	"applet>".replace("<", "&lt;").replace(">", "&gt;")
	,	"<embed".replace("<", "&lt;").replace(">", "&gt;")
	,	"embed>".replace("<", "&lt;").replace(">", "&gt;")
	,	"<form".replace("<", "&lt;").replace(">", "&gt;")
	,	"form>".replace("<", "&lt;").replace(">", "&gt;")
	,	"<input".replace("<", "&lt;").replace(">", "&gt;")
	,	"<img".replace("<", "&lt;").replace(">", "&gt;")
	,	"<a".replace("<", "&lt;").replace(">", "&gt;")
	,	"</a>".replace("<", "&lt;").replace(">", "&gt;")
	
	,	"src"
	,	"href"
	,	"onload"
	,	"onclick"
	,	"onerror"
	,	"window."
	,	"document."
	,	"alert"
	,	"prompt"
	,	"href"
	,	"htm"
	,	"|"
	,	"$"
	,	"%"
	};
	private final static String REPLACEMENT = "";
	
	public static boolean containsIgnoreCase(String str, String searchStr) {
		if(str == null || searchStr == null)	return false;
		int length = searchStr.length();
		if (length == 0)	return true;
		for (int i = str.length() - length; i >= 0; i--) {
			if (str.regionMatches(true, i, searchStr, 0, length))
				return true;
		}
		return false;
	}
	public static String getXssCleanStr( String paramValue ) {
		if(paramValue == null || paramValue.length() < 1)	return paramValue;
		String temp = paramValue;
		boolean containsAfterChange = false;
		for (String target : SEARCHES) {
			if (containsIgnoreCase(paramValue, target)) {
				temp = temp.replaceAll("(?i)" + target, REPLACEMENT);	//replace ignorecase using regex.
			}
			if (!containsAfterChange) {
				containsAfterChange = containsIgnoreCase(temp, target);
			}
		}
		return	containsAfterChange
			?	getXssCleanStr(temp)
			:	temp
				.replace("&lt;", "<")	//에디터일 경우 이미 치환되서 옴
				.replace("&gt;", ">")	//에디터일 경우 이미 치환되서 옴
				.replace("&", "&amp;")
				.replace("<", "&lt;")
				.replace(">", "&gt;")
				.replace("#", "&#35;")
				.replace("(", "&#40;")
				.replace(")", "&#41;")
				.replace("'", "&apos;")
				.replace("\"", "&quot;");
	}
}