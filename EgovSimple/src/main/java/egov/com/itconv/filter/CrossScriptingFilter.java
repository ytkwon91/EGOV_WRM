package egov.com.itconv.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.*;

public class CrossScriptingFilter implements Filter {
	private final static Logger LOGGER = LoggerFactory.getLogger(CrossScriptingFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		LOGGER.debug("init.");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if(request instanceof HttpServletRequest) {
			HttpServletRequest req = (HttpServletRequest) request;
			
			String uri = req.getRequestURI();
			if(StringUtils.startsWith(uri, "/unauthenticated")) {
				//URI 패턴 잡아서
				//XSS 처리할 URI만 필터 걸자
				LOGGER.debug("doFilter.");
				chain.doFilter(new RequestWrapper(req), response);
			}
			
		}
	}

	@Override
	public void destroy() {
		LOGGER.debug("destroy.");
	}

}
